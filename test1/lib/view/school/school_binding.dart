import 'package:get/get.dart';
import 'package:test1/view/school/school_controller.dart';

class SchoolBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<SchoolController>(SchoolController());
  }
}
