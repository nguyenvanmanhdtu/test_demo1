import 'package:flutter/material.dart';
import 'package:test1/router/splash_routes.dart';
import 'package:test1/view/school/school_controller.dart';
import 'package:test1/view/student/list_student_page.dart';
import 'package:get/get.dart';

class SchoolPage extends GetView<SchoolController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Trường ĐH',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center),
      ),
      body: GetBuilder(
        init: SchoolController(),
        builder: (SchoolController controller) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                MaterialButton(
                  height: 40,
                  onPressed: () {
                    Get.toNamed(SplashRoutes.STUDENT);
                  },
                  color: Colors.red,
                  child: const Text(
                    "kích vào button",
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
