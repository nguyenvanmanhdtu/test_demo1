import 'package:get/get.dart';
import 'package:test1/view/student/list_student_controller.dart';

class ListStudentBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListStudentController>(() => ListStudentController());
  }
}
