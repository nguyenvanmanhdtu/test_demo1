import 'package:get/get.dart';
import 'package:test1/model/class/class_response.dart';
import 'package:test1/model/student/student_response.dart';
import 'package:test1/provider/Student_provider.dart';

import '../../router/splash_routes.dart';

class ListStudentController extends GetxController {
  ///
  /// Get Go To Screen.
  ///
  void getGotoScreen() {
    Get.toNamed(SplashRoutes.CREATE_SCHOOL);
  }

  ///
  /// Update Go To Screen.
  ///
  void updateGotoScreen(String id) {
    Get.toNamed(SplashRoutes.UPDATE_SCHOOL, arguments: id)?.then((value) {
      students?.where((element) => element.id == value?.id).forEach((element) {
        element.fullName = value.fullName;
        element.numberphone = value.numberphone;

        // students?.add(value);
        update();
      });
      // xoa
      // students?.removeWhere((element) => element.id == id);
      update();
    });
    update();
  }

  List<StudentResponse>? students;

  @override
  void onInit() async {
    super.onInit();

    await studentProvider().get().then(
      (value) async {
        if (value.isNotEmpty) {
          List<ClassResponse> classStudent = await studentProvider().getClass();
          students = value.map((e) {
            final classData =
                classStudent.firstWhere((element) => element.id == e.id);

            return e.copyWith(
              id: classData.id,
              classs: classData,
            );
          }).toList();
        }
      },
    ).catchError((onError) {
      print(onError);
    });
    update();

    // print("datat ${data[4].fullName}");

    // final postdat = studentProvider().post(StudentRequest(
    //     fullName: "abc", address: "dn", numberphone: "00324023"));

    // studentProvider().put(StudentRequest(
    //     id: "3",
    //     fullName: "da nang sg",
    //     address: "komtum",
    //     numberphone: '0774892320'));
  }
}
