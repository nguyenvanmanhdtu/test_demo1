import 'package:get/get.dart';
import 'package:test1/view/student/create_student/create_student_controller.dart';

class CreateStudentBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<CreateStudentController>(CreateStudentController());
  }
}
