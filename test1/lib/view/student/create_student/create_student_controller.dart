import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreateStudentController extends GetxController {
// aviable.
  String nameSchool = "";
  String fullName = "";
  String nameClass = "";
  String address = "";
  String numberphone = "";

  ///
  /// On Change Name School.
  ///
  void onChangeNameSchool(String val) {
    nameSchool = val;
    update();
  }

  ///
  /// On Change Full Name.
  ///
  void onChangeFullName(String val) {
    fullName = val;
    update();
  }

  ///
  /// On Change Name Class.
  ///
  void onChangeNameClass(String val) {
    nameClass = val;
    update();
  }

  ///
  /// On Change Address.
  ///
  void onChangeAddress(String val) {
    address = val;
    update();
  }

  ///
  /// On Change Number Phone.
  ///
  void onChangeNumberphone(String val) {
    fullName = val;
    update();
  }

  ///
  /// Create Button.
  ///
  void createButton() {}
}
