import 'package:get/get.dart';
import 'package:test1/view/student/update_student.dart/update_studen_controller.dart';

class UpdateStudentBinding extends Bindings {
  @override
  void dependencies() {
    Get.put<UpdateStudentController>(UpdateStudentController());
  }
}
