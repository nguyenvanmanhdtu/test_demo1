import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:test1/model/student/student_request.dart';

import '../../../model/student/student_response.dart';
import '../../../provider/Student_provider.dart';

class UpdateStudentController extends GetxController {
// aviable.
  String nameSchool = "";
  String fullName = "";
  String nameClass = "";
  String address = "";
  String numberphone = "";
  TextEditingController fullNametxt = TextEditingController();
  TextEditingController nameClasstxt = TextEditingController();
  TextEditingController nameSchooltxt = TextEditingController();
  TextEditingController addresstxt = TextEditingController();
  TextEditingController numberphonetxt = TextEditingController();

  String? arg;
  @override
  void onInit() {
    super.onInit();
    arg = Get.arguments as String;
    print("data ${Get.arguments}");
  }

  ///
  /// On Change Name School.
  ///
  void onChangeNameSchool(String val) {
    nameSchool = val;
    update();
  }

  ///
  /// On Change Full Name.
  ///
  void onChangeFullName(String val) {
    fullName = val;
    update();
  }

  ///
  /// On Change Name Class.
  ///
  void onChangeNameClass(String val) {
    nameClass = val;
    update();
  }

  ///
  /// On Change Address.
  ///
  void onChangeAddress(String val) {
    address = val;
    update();
  }

  ///
  /// On Change Number Phone.
  ///
  void onChangeNumberphone(String val) {
    fullName = val;
    update();
  }

  List<StudentResponse>? students;

  ///
  /// Edit Student.
  ///
  void editStudent(String? id) {
    final student = StudentRequest(
      id: id,
      fullName: fullNametxt.text,
      address: 'wwww',
      numberphone: 'rrrrr',
    );

    // goi api
    studentProvider().put(student);

    Get.back(result: student);
  }

  ///
  /// Delete Student.
  ///
  void deleteStudent() {}
}
