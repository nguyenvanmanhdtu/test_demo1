import 'dart:convert';

import 'package:test1/held/validate.dart';

class SchoolResponse {
  String? id;
  String? nameSchool;
  String? address;
  SchoolResponse({this.id, this.nameSchool, this.address});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(nameSchool)) 'nameSchool': nameSchool,
      if (!IZIValidate.nullOrEmpty(address)) 'address': address,
    };
  }

  factory SchoolResponse.fromMap(Map<String, dynamic> json) {
    return SchoolResponse(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      nameSchool: IZIValidate.nullOrEmpty(json['nameSchool'])
          ? null
          : json['nameSchool'].toString(),
      address: IZIValidate.nullOrEmpty(json['address'])
          ? null
          : json['address'].toString(),
    );
  }

  String toJson() => json.encode(toMap());

  factory SchoolResponse.fromJson(String source) =>
      SchoolResponse.fromMap(json.decode(source) as Map<String, dynamic>);
}
