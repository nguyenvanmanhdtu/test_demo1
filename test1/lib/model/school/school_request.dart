import 'dart:convert';

import 'package:test1/held/validate.dart';

class SchoolRequest {
  String? id;
  String? nameClass;
  String? address;
  SchoolRequest({this.id, this.nameClass, this.address});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(nameClass)) 'nameClass': nameClass,
      if (!IZIValidate.nullOrEmpty(nameClass)) 'address': address,
    };
  }

  factory SchoolRequest.fromMap(Map<String, dynamic> json) {
    return SchoolRequest(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      nameClass: IZIValidate.nullOrEmpty(json['nameClass'])
          ? null
          : json['nameClass'].toString(),
      address: IZIValidate.nullOrEmpty(json['address'])
          ? null
          : json['address'].toString(),
    );
  }

  String toJson() => json.encode(toMap());

  factory SchoolRequest.fromJson(String source) =>
      SchoolRequest.fromMap(json.decode(source) as Map<String, dynamic>);
}
