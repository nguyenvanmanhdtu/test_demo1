import 'dart:convert';

import 'package:test1/held/validate.dart';

class ClassRequest {
  String? id;
  String? nameClass;

  ClassRequest({
    this.id,
    this.nameClass,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(nameClass)) 'nameClass': nameClass,
    };
  }

  factory ClassRequest.fromMap(Map<String, dynamic> json) {
    return ClassRequest(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      nameClass: IZIValidate.nullOrEmpty(json['nameClass'])
          ? null
          : json['nameClass'].toString(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ClassRequest.fromJson(String source) =>
      ClassRequest.fromMap(json.decode(source) as Map<String, dynamic>);
}
