import 'dart:convert';

import 'package:test1/held/validate.dart';

class ClassResponse {
  String? id;
  String? nameClass;
  ClassResponse({
    this.id,
    this.nameClass,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(nameClass)) 'nameClass': nameClass,
    };
  }

  factory ClassResponse.fromMap(Map<String, dynamic> json) {
    return ClassResponse(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      nameClass: IZIValidate.nullOrEmpty(json['nameClass'])
          ? null
          : json['nameClass'].toString(),
    );
  }

  String toJson() => json.encode(toMap());

  factory ClassResponse.fromJson(String source) =>
      ClassResponse.fromMap(json.decode(source) as Map<String, dynamic>);
}
