import 'dart:convert';
import 'package:test1/held/validate.dart';

class StudentRequest {
  String? id;
  String? fullName;
  String? address;
  String? numberphone;
  StudentRequest({
    this.id,
    this.fullName,
    this.address,
    this.numberphone,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(fullName)) 'fullName': fullName,
      if (!IZIValidate.nullOrEmpty(address)) 'address': address,
      if (!IZIValidate.nullOrEmpty(numberphone)) 'numberphone': numberphone,
    };
  }

  factory StudentRequest.fromMap(Map<String, dynamic> json) {
    return StudentRequest(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      fullName: IZIValidate.nullOrEmpty(json['fullName'])
          ? null
          : json['fullName'].toString(),
      address: IZIValidate.nullOrEmpty(json['address'])
          ? null
          : json['address'].toString(),
      numberphone: IZIValidate.nullOrEmpty(json['numberphone'])
          ? null
          : json['numberphone'].toString(),
    );
  }

  String toJson() => json.encode(toMap());

  factory StudentRequest.fromJson(String source) =>
      StudentRequest.fromMap(json.decode(source) as Map<String, dynamic>);
}
