import 'dart:convert';

import 'package:test1/held/validate.dart';
import 'package:test1/model/class/class_response.dart';

class StudentResponse {
  String? id;
  String? fullName;
  String? address;
  String? numberphone;
  ClassResponse? classs;
  StudentResponse(
      {this.id, this.fullName, this.address, this.numberphone, this.classs});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      if (!IZIValidate.nullOrEmpty(id)) 'id': id,
      if (!IZIValidate.nullOrEmpty(fullName)) 'fullName': fullName,
      if (!IZIValidate.nullOrEmpty(address)) 'address': address,
      if (!IZIValidate.nullOrEmpty(numberphone)) 'numberphone': numberphone,
    };
  }

  factory StudentResponse.fromMap(Map<String, dynamic> json) {
    return StudentResponse(
      id: IZIValidate.nullOrEmpty(json['id']) ? null : json['id'].toString(),
      fullName: IZIValidate.nullOrEmpty(json['fullName'])
          ? null
          : json['fullName'].toString(),
      address: IZIValidate.nullOrEmpty(json['address'])
          ? null
          : json['address'].toString(),
      numberphone: IZIValidate.nullOrEmpty(json['numberphone'])
          ? null
          : json['numberphone'].toString(),
    );
  }

  StudentResponse copyWith({
    String? id,
    String? fullName,
    String? address,
    String? numberphone,
    ClassResponse? classs,
  }) {
    return StudentResponse(
      id: id ?? this.id,
      fullName: fullName ?? this.fullName,
      address: address ?? this.address,
      numberphone: numberphone ?? this.numberphone,
      classs: classs ?? this.classs,
    );
  }

  String toJson() => json.encode(toMap());

  factory StudentResponse.fromJson(String source) =>
      StudentResponse.fromMap(json.decode(source) as Map<String, dynamic>);
}
