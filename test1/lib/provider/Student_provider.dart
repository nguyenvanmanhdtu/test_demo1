import 'package:dio/dio.dart';
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:test1/model/class/class_response.dart';
import 'package:test1/model/student/student_request.dart';
import 'package:test1/model/student/student_response.dart';

class studentProvider {
  Future<List<StudentResponse>> get() async {
    try {
      Dio dio = Dio();
      final data = await dio.get(
        "https://63e07f598b24964ae0ff024d.mockapi.io/api/student",
        // data: {}
      );
      final resul = data.data as List<dynamic>;

      return resul
          .map((e) => StudentResponse.fromMap(e as Map<String, dynamic>))
          .toList();
    } on FormatException catch (_) {
      throw const FormatException('Unable to process the data');
    } catch (e) {
      rethrow;
    }
  }

  Future<List<ClassResponse>> getClass() async {
    try {
      Dio dio = Dio();
      final data = await dio.get(
        "https://63e07f598b24964ae0ff024d.mockapi.io/api/class",
        // data: {}
      );
      final resul = data.data as List<dynamic>;
      // print("ahah ${resul}");

      return resul
          .map((e) => ClassResponse.fromMap(e as Map<String, dynamic>))
          .toList();
    } on FormatException catch (_) {
      throw const FormatException('Unable to process the data');
    } catch (e) {
      rethrow;
    }
  }

  Future<StudentResponse> post(StudentRequest studentRequest) async {
    try {
      Dio dio = Dio();
      final data = await dio.post(
          "https://63e07f598b24964ae0ff024d.mockapi.io/api/student",
          data: studentRequest.toMap());
      final resul = data.data as List<StudentResponse>;
      print("ahah ${data}");

      return StudentResponse.fromMap(resul as Map<String, dynamic>);
    } on FormatException catch (_) {
      throw const FormatException('Unable to process the data');
    } catch (e) {
      rethrow;
    }
  }

  Future<StudentResponse> put(StudentRequest studentRequest) async {
    try {
      Dio dio = Dio();
      final data = await dio.put(
          "https://63e07f598b24964ae0ff024d.mockapi.io/api/student/${studentRequest.id}",
          data: studentRequest.toMap());
      final resul = data.data as dynamic;
      print("ahah ${data}");

      return StudentResponse.fromMap(resul as Map<String, dynamic>);
    } on FormatException catch (_) {
      throw const FormatException('Unable to process the data');
    } catch (e) {
      rethrow;
    }
  }
}
