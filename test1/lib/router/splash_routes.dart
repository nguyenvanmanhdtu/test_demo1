import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:test1/view/school/school_binding.dart';
import 'package:test1/view/school/school_page.dart';
import 'package:test1/view/student/create_student/create_student_binding.dart';
import 'package:test1/view/student/create_student/create_student_page.dart';
import 'package:test1/view/student/list_student_binding.dart';
import 'package:test1/view/student/list_student_page.dart';
import 'package:test1/view/student/update_student.dart/update_studen_page.dart';
import 'package:test1/view/student/update_student.dart/update_student_binding.dart';

// ignore: avoid_classes_with_only_static_members
class SplashRoutes {
  static const String STUDENT = '/student';
  static const String SCHOOL = '/school';
  static const String CREATE_SCHOOL = '/create_school';
  static const String UPDATE_SCHOOL = '/update_school';
  static List<GetPage> list = [
    GetPage(
      name: STUDENT,
      page: () => ListStudentPage(),
      binding: ListStudentBinding(),
    ),
    GetPage(
      name: SCHOOL,
      page: () => SchoolPage(),
      binding: SchoolBinding(),
    ),
    GetPage(
      name: CREATE_SCHOOL,
      page: () => CreateStudentPage(),
      binding: CreateStudentBinding(),
    ),
    GetPage(
      name: CREATE_SCHOOL,
      page: () => CreateStudentPage(),
      binding: CreateStudentBinding(),
    ),
    GetPage(
      name: UPDATE_SCHOOL,
      page: () => UpdateStudentPage(),
      binding: UpdateStudentBinding(),
    )
  ];
}
