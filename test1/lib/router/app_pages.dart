import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:test1/router/splash_routes.dart';

// ignore: avoid_classes_with_only_static_members
class AppPages {
  static List<GetPage> list = [
    ...SplashRoutes.list,
  ];
}
