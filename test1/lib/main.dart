import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';
import 'package:test1/app_binding.dart';
import 'package:test1/router/app_pages.dart';
import 'package:test1/router/splash_routes.dart';

import 'app_constants.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: SplashRoutes.SCHOOL,
      initialBinding: AppBinding(),
      defaultTransition: Transition.noTransition,
      transitionDuration: const Duration(),
      getPages: AppPages.list,
      debugShowCheckedModeBanner: false,
      localizationsDelegates: localizationsDelegates,
    );
  }
}
